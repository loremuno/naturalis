import { NgModule } from '@angular/core';

import { BodyComponent } from './body.component';

import { HeaderComponent } from './header/header.component';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { ServicesComponent } from './services/services.component';
import { ContactComponent } from './contact/contact.component';
import { TeamComponent } from './team/team.component';
import { ClientsComponent } from './clients/clients.component';
import { AboutComponent } from './about/about.component';
import { PricesModalComponent } from './prices-modal/prices-modal.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    BodyComponent,
    HeaderComponent,
    PortfolioComponent,
    ServicesComponent,
    ContactComponent,
    TeamComponent,
    ClientsComponent,
    AboutComponent,
    PricesModalComponent
  ],
  exports: [BodyComponent],
  imports: [
    HttpClientModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: []
})
export class BodyModule { }
