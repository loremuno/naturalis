import { Component, OnInit } from "@angular/core";
import * as $ from "jquery";

@Component({
  selector: "app-portfolio",
  templateUrl: "./portfolio.component.html",
  styleUrls: ["./portfolio.component.css"]
})
export class PortfolioComponent implements OnInit {
  constructor() {}

  ngOnInit() {
    // Hide navbar when modals trigger
    $(".portfolio-modal").on("show.bs.modal", function(e) {
      $(".navbar").addClass("d-none");
    });
    $(".portfolio-modal").on("hidden.bs.modal", function(e) {
      $(".navbar").removeClass("d-none");
    });
  }
}
