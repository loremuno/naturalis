import { Component, OnInit } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: "app-contact",
  templateUrl: "./contact.component.html",
  styleUrls: ["./contact.component.css"]
})
export class ContactComponent implements OnInit {
  name: string;
  email: string;
  phone: string;
  message: string;

  constructor(
    private readonly http: HttpClient,
  ) {}

  ngOnInit() {}

  sendMail() { 
    event.preventDefault();
    this.http.post('https://us-central1-naturalis-2ebf2.cloudfunctions.net/sendMail', {}).subscribe();
  }
}
