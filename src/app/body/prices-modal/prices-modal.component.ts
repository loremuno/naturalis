import { Component, OnInit } from "@angular/core";
import * as $ from "jquery";

@Component({
  selector: "app-prices-modal",
  templateUrl: "./prices-modal.component.html",
  styleUrls: ["./prices-modal.component.css"]
})
export class PricesModalComponent implements OnInit {
  constructor() {}

  ngOnInit() {
    $('#myModal').on('shown.bs.modal', function () {
      $('#myInput').trigger('focus')
    })
  }
}
